from github import Github, GithubException
import json
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta


g = Github("YOUR API KEY")


def increment_month(x):
    return datetime.strptime((x + relativedelta(months=+1)).strftime('%Y-%m-%d'), '%Y-%m-%d')

def date_to_string(x):
    return x.strftime('%Y-%m-%d')

def get_repo_content(repo):
    return repo.get_contents("")

def get_repo_file(contents):
    return contents.pop(0)

def explore_sub_dir_2(repo, path):
    return repo.get_contents(path)

#This function increments the number of requests and when it's surpassed waits an hour.
#Limited to 5000 searches per hour
def check_rate(val):
    if(val<4990):
        return val+1
    else:
        print("Waiting an hour")
        print("1/6")
        time.sleep(600)
        print("2/6")
        time.sleep(600)
        print("3/6")
        time.sleep(600)
        print("4/6")
        time.sleep(600)
        print("5/6")
        time.sleep(600)
        print("6/6")
        time.sleep(600)
        return 0
    

#If you give this function the json file that already exists it will continue where it last stopped
#The parameter start needs to follow the syntax '%Y-%m-%d'
def search(name='wf_crawl.json', start = '2017-01-01'):
    #Define these variables so we can see the progess
    nb = 0 
    #Getting the current date so that the function knows when to stop
    today= datetime.today()

    #Setting the date to start searching
    date_working= datetime.strptime(start, '%Y-%m-%d')

    #Defining the structure to save the data and right the json file later on
    wf_crawl = {}
    #Recuparating the data that already exist to continue where we left off
    #If the file is in the same directory 
    try:
        with open(name) as json_file:
            #Read the file
            json_data = json.load(json_file)
            #Recuparate the last working date
            date_working = datetime.strptime(json_data['last_date']['date'][0], '%Y-%m-%d')
            #Remove the working date
            json_data.pop('last_date', None)
            #Set the data
            wf_crawl= json_data.copy()
    except:
        print("Either the file doesn't exist or I coudn't read it. So i'm starting from 0.")
    val=0
    
    #For every date till today
    while(date_working<today):
        #Updating the day since it's possible that the crawler takes longer than a day
        today= datetime.today()
        #Getting the time interval to search
        first_date= date_to_string(date_working)
        
        date_working= increment_month(date_working)
        second_date= date_to_string(date_working)
        #Defining the

        #https://docs.github.com/en/search-github/getting-started-with-searching-on-github/understanding-the-search-syntax
        query=f"nextflow created:{first_date}..{second_date}"

        #Retrieving the repositories
        result = g.search_repositories(query)
        val=check_rate(val)
        #Getting the number of repositories
        nb_result= result.totalCount
        #Adding them to the total
        nb+=nb_result
        #index
        index=0
        #Print
        print(f'Between {first_date} and {second_date} : ')
        #For each repositories
        for repo in result:
            index+=1
            print(f'Working on {index}/{nb_result}')

            try :
                # 1st request to GitHub
                #Getting the contents of the repositories 
                #Check if there is an .nf file at the root of the project
                #This is a way to check is the repo decribes a pipeline or not
                nextflow_file_in_root = False
                content = repo.get_contents("")
                for file in content:
                    if(file.path[-3:]=='.nf'):
                        nextflow_file_in_root = True

                val=check_rate(val)
                if(nextflow_file_in_root):
                     #Checking if there is a License
                     if(repo.license!=None):
                        
                        #Adding the different information to the json file
                        wf_crawl[repo.full_name] = {}
                        #Name of the workflow
                        #Since the format given is : 'owner/name'
                        wf_crawl[repo.full_name]['name'] = str(repo.full_name).split('/')[1]
                        #Creation date
                        wf_crawl[repo.full_name]['creation_date'] = date_to_string(repo.created_at)
                        #Actual date (date when creating the json)
                        wf_crawl[repo.full_name]['actual_date'] = date_to_string(today)
                        #Owner
                        wf_crawl[repo.full_name]['owner'] = repo.owner.login
                        #Contributors
                        wf_crawl[repo.full_name]['contributors'] = repo.contributors_url
                        #Description
                        wf_crawl[repo.full_name]['description'] = repo.description
                        #Nb forks
                        wf_crawl[repo.full_name]['forks'] = repo.forks
                        #Nb stars
                        wf_crawl[repo.full_name]['stars'] = repo.stargazers_count
                        #License
                        wf_crawl[repo.full_name]['license'] = repo.license.name
                        #Commits
                        val=check_rate(val)
                        commits = list(repo.get_commits())
                        val=check_rate(val)
                        wf_crawl[repo.full_name]['last_commit_date'] = date_to_string(commits[0].commit.author.date)
                        val=check_rate(val)
                        wf_crawl[repo.full_name]['last_commit_id'] = str(commits[0].commit._identity)
                        val=check_rate(val)
                        wf_crawl[repo.full_name]['first_commit_date'] = date_to_string(commits[-1].commit.author.date)
                        val=check_rate(val)
                        wf_crawl[repo.full_name]['first_commit_id'] = str(commits[-1].commit._identity)
                        val=check_rate(val)
                        wf_crawl[repo.full_name]['nb_commmits'] = len(commits)
                        val=check_rate(val)
                else:
                    nb=-1
                    
            except GithubException as error:
                print(error)
                break
        #Showing the progress
        print(f'Between {first_date} and {second_date}, found {nb_result} repo(s), total repo(s): {nb}')

        #Witing the results into the json file
        #Writting the json file
        with open(name, 'w') as output_file :
            #We add the last last working date to be able continue where the crawler last stopped
            temp = wf_crawl.copy()
            last_date= second_date
            if(date_working>today):
                last_date =date_to_string(today)
            temp['last_date'] = {'date':[last_date]}
            json.dump(temp, output_file, indent=4)
    


