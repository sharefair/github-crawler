# Github-Crawler


[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-green.svg)](https://www.gnu.org/licenses/gpl-3.0) 

## Description

This repository contains the code composing a crawler adapted to search for Nextflow workflows on Github.

The GitHub Search API has a custom rate limit. User-to-server requests are limited to 5,000 requests per hour per authenticated user. The crawler performs the 5,000 requests linearly and is not limited by time; however, after the 5,000 requests, the crawler waits for an hour.

The GitHub Search API provides up to 1,000 results for each search, which can be problematic when attempting to compile a large corpus, such as workflows. To circumvent this limitation, the global request to `retrieve Nextflow repositories` is subdivided into multiple requests, each focusing on a specific time frame, such as `retrieve Nextflow repositories between date 1 and date 2`. This process involves incrementing the months and years automatically, ensuring comprehensive coverage of the desired repositories.

While the crawler is running, the data is saved in a JSON file.

> It's important to note that due to GitHub's search functionality, the results of a search may not be robust, thus the crawler's results may not be reproducible.

> The current version of the crawler is adapted to work on Nextflow workflows and workflows with at least one Nextflow file at the root of the project. However, it is easy to adapt the crawler to a more generic functionality.


## Table of Contents

- [Github-Crawler](#github-crawler)
  - [Description](#description)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
  - [License](#license)

## Installation

The python function dependancies are described in the `requirements.txt` file.

## License

This project is licensed under the [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.en.html).

___

<img align="left" src="img/paris_saclay.png" width="20%">
<img align="left" src="img/lisn.png" width="20%">
<img align="left" src="img/pasteur.png" width="20%">
<img align="left" src="img/sharefair.png" width="20%">
<img align="left" src="img/france2030.png" width="20%">

<br/><br/>
<br/><br/>
<br/><br/>

